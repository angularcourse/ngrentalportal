import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CoursesService {

  courses = [
    { id: 11, name: 'Angular', price:45.569, ratings: 4.9333,
    releaseDate: new Date(2021, 4, 4), 
    description: 'Learn one way to build applications with Angular and reuse your code and abilities to build apps for any deployment target. For web, mobile web, native mobile and native desktop.Achieve the maximum speed possible on the Web Platform today, and take it further, via Web Workers and server-side rendering.' , isBookmarked: false},
    { id: 2, name: 'Nestjs', price:545.563, ratings: 4.8233, releaseDate: new Date(2021, 2, 4), isBookmarked: false, 
    description: 'Learn one way to build applications with Angular and reuse your code and abilities to build apps for any deployment target. For web, mobile web, native mobile and native desktop.Achieve the maximum speed possible on the Web Platform today, and take it further, via Web Workers and server-side rendering.' },
    { id: 31, name: 'MongoDb', price:49.516, ratings: 4.711, releaseDate: new Date(2021, 3, 4), isBookmarked: false,
    description: 'Learn one way to build applications with Angular and reuse your code and abilities to build apps for any deployment target. For web, mobile web, native mobile and native desktop.Achieve the maximum speed possible on the Web Platform today, and take it further, via Web Workers and server-side rendering.' },
    { id: 43, name: 'Nodejs', price:945.256,releaseDate: new Date(2021, 1, 4),  ratings: 4.3,isBookmarked: false,
    description: 'Learn one way to build applications with Angular and reuse your code and abilities to build apps for any deployment target. For web, mobile web, native mobile and native desktop.Achieve the maximum speed possible on the Web Platform today, and take it further, via Web Workers and server-side rendering.' },
    
]
  constructor() { }

  getCourses(){
    return this.courses; 
  }

  addCourse(course){
    this.courses.push(course);
  }

  removeCourse(course){
    let index = this.courses.indexOf(course);
    this.courses.splice(index, 1)
  }
}
