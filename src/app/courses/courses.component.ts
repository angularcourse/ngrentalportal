import { Component, OnInit, SimpleChanges } from '@angular/core';
import { CoursesService } from './courses.service';

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css'],
})
export class CoursesComponent implements OnInit {
 
  courses: any[];
  

  constructor(private service: CoursesService){
     console.log('constructor initialized')
  }

  ngOnInit(){
    console.log('ngOnInit')
      this.courses = this.service.getCourses()
  }

  onKeyUp($event){  
    // console.log('$event', $event);
    if($event.keyCode === 13)
      console.log('Value on Enter is ', $event.target.value);

  }

  onAddressKeyUp(value){
    console.log('value', value);

  }

  bookmarkChange(eventArgs : { isBookmarked: boolean}){
    console.log('isBookmarked change ', eventArgs)
  }

  trackByCourses(index: number, course: any) : number 
  { return course? course.id : undefined}

  addNewCourse(){
    this.service.addCourse(
      {
        id: 8, 
        name: 'SpringBoot', 
        isBookmarked: false
      }
    );
  }

  removeCourse(course){
    this.service.removeCourse(course);
  }


}
