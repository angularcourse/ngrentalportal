import { ThrowStmt } from '@angular/compiler';
import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appInputFormat]'
})
export class InputFormatDirective {

  @Input('appInputFormat')  format;

  constructor(private el: ElementRef) { }

  @HostListener('blur') onBlur() {
      let value: string = this.el.nativeElement.value;
      console.log('value is ', value)

      this.format === 'uppercase' ?
      this.el.nativeElement.value = value.toUpperCase()
      :
      this.el.nativeElement.value = value.toLowerCase()
  }

}
