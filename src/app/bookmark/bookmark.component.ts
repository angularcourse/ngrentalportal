import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-bookmark',
  templateUrl: './bookmark.component.html'
})
export class BookmarkComponent implements OnInit {

  @Input('isBookmarked') isBookmarked: boolean = false;
  @Input('courseName') courseName: string;
  @Output('change') change = new EventEmitter();
  constructor() { }


  ngOnInit(): void {
    console.log('isBookmarked ...')
  }


  bookMarkDivClicked(){
    console.log('bookMark Div Clicked')
  }

  bookmarkTheCourse(event){

    event.stopPropagation();

    console.log('bookMark Button Clicked')
    this.isBookmarked = !this.isBookmarked
    this.change.emit({isBookmarked: this.isBookmarked})
  }

}
