import { RouterModule } from '@angular/router';
import { SummaryPipe } from './courses/summary.pipe';
import { MaterialModule } from './material/material.module';
import { AppRoutingModule } from './app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CoursesComponent }  from  './courses/courses.component'
import { from } from 'rxjs';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BookmarkComponent } from './bookmark/bookmark.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InputFormatDirective } from './input-format.directive';
import { ContactFormComponent } from './contact-form/contact-form.component';
import { SignupFormComponent } from './signup-form/signup-form.component';
import { HttpClientModule }  from '@angular/common/http';
import { PostsComponent } from './posts/posts.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CoursesService } from './courses/courses.service';

@NgModule({
  declarations :[ 
    AppComponent,
    CoursesComponent,
    BookmarkComponent,
    InputFormatDirective ,
    SummaryPipe,
    ContactFormComponent,
    SignupFormComponent,
    PostsComponent,
    NavbarComponent
  ],
  imports: [ 
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
  ],
  exports: [],
  providers: [CoursesService],
  bootstrap: [ AppComponent]
})
export class AppModule {}