import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title: string = 'ANGULAR 10/11';
  name: string = 'John Doe';
  bufferValue: number =40
  progressValue: number = 20

  format = 'lowercase';
  flag=true;

  paymentMode = 'IMPS'
  
  imgUrl: string =
    'https://intellipaat.com/course-image/2020/11/Intellipaat-logo.webp';

  constructor() {}

  ngOnInit(): void {}

  clickMe() {
    console.log('Button has been tapped. Event Binding');
  }

  changeInName(event){
    console.log('change....', event.target.value)
      this.name = event.target.value;
  }


  @HostListener("window:scroll", [])
  onWindowScroll(){
    console.log('window is scrolling...')
    let element = document.documentElement
    let body = document.body;
    let scrollTop = 'scrollTop';
    let scrollHeight = 'scrollHeight';

    this.progressValue = 
    (element[scrollTop] || body[scrollTop]) 
    / ( (element[scrollHeight] || body[scrollHeight])  - element.clientHeight) *100
  }
  
}
