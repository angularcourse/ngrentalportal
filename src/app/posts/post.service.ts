import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError }  from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class PostService {

  postsUrl = 'https://jsonplaceholder.typicode.com/posts';

  constructor(private httpClient: HttpClient) { }



  getPosts = () => {
    return this.httpClient.get(this.postsUrl)
  }

  getPostDetail = (postId) => {
    return this.httpClient.get(this.postsUrl+'/'+postId);
  }
  createPost = (post) => {
    return this.httpClient.post(this.postsUrl, 
      JSON.stringify(post)
      )
  }

  updatePost = (post)=>{
    let url = this.postsUrl+'/'+post.id;
    console.log('url ', url);
    this.httpClient.patch(url, JSON.stringify({isRead: true}));
     return this.httpClient.patch(url, JSON.stringify(post))
  }


  deletePost = (post)=> {
    let url = this.postsUrl+'/'+post.id;
      console.log('url ', url);
      return this.httpClient.delete(url).pipe(
        catchError( val => {  
          console.log('catch error', val);
         return null;})
      );
  }
}
