import { PostService } from './post.service';
import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts = []
  
  constructor(private postService: PostService) { }

  ngOnInit(): void {
    this.getPosts();
  }

  
  getPosts = () => {
    this.postService.getPosts()
    .subscribe( (response : [any]) => {
      console.log(response)
      this.posts = response;      
    },
    (error: Response) => {
      
        if(error.status === 404)
              alert('Url is not found')
        else
        if(error.status === 500)
          alert('Internal Server Error')
        else
          alert('An unexpected error has occurred');
          console.log('error', error);

    });
  }

  addNewPost = (input: HTMLInputElement) => {
        console.log('title ', input.value);
        let post = {
          title: input.value, 
          body: input.value, 
          userId: 1}
    this.postService.createPost(post)
        .subscribe( response => {
          console.log('response', response)
          input.value = ''
          post['id'] = response['id'];
          this.posts.splice(0,0,post)
        });

  }

  updatePost = (post) => {
     this.postService.updatePost(post).subscribe(response => console.log('patch update', response));
  }


  deletePost = (post)=> {
      this.postService.deletePost(post)
      .subscribe(response => {
        let postIndex = this.posts.indexOf(post);
        this.posts.splice(postIndex,1);
      });   
  }
}
