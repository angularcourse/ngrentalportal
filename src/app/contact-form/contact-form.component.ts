import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css']
})
export class ContactFormComponent implements OnInit {

  fname: string;
  comment: string;

  contactMethods = [
    {id: 1, name: 'Email'},
    {id: 2, name: 'Phone'},
    {id: 3, name: 'Sms'}
  ]

  constructor() { }

  ngOnInit(): void {
  }

  logName(model){
    console.log('model', model);
    
    console.log('fname', model.control.value)
  }

  submitForm(formObj){
    console.log('form obj', formObj.value);
  }

}
