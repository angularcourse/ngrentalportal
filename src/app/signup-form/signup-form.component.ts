import { UsernameValidators } from './username.validators';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.css'],
})
export class SignupFormComponent implements OnInit {
  myFormGroup: FormGroup;


  // myFormGroup = new FormGroup({
  //   account: new FormGroup({ 
  //     username: new FormControl('', 
  //     [
  //       Validators.required,
  //       Validators.minLength(4),
  //       UsernameValidators.cannotContainSpace
  //     ],
  //       UsernameValidators.isUsernameAlreadyExists
  //     ),
  //     password: new FormControl('', [Validators.required]), 
  //     }),
  //     contactMethods : new FormArray([])
  // });

  constructor(private fb: FormBuilder) {
    this.myFormGroup = fb.group({
      account: fb.group({
        username: ['', [
          Validators.required,
          Validators.minLength(4),
          UsernameValidators.cannotContainSpace
        ],
        UsernameValidators.isUsernameAlreadyExists
      ],
      password: ['', [Validators.required]]
      }),
      contactMethods: fb.array([])
    })
  }

  ngOnInit(): void {}

  usernameChange() {
    console.log(this.username);
  }
  get username(): FormControl {
    return this.myFormGroup.get('account.username') as FormControl;
  }

  get password(): FormControl {
    return this.myFormGroup.get('account.password') as FormControl;
  }


  get contactMethods(): FormArray{
    return this.myFormGroup.get('contactMethods') as FormArray;
  }

  addContactMethod(method: HTMLInputElement){
      this.contactMethods.push(new FormControl(method.value));
      method.value = ''
  }

  removeMethod(method:FormControl) {
     let pos = this.contactMethods.controls.indexOf(method);
     this.contactMethods.removeAt(pos);
  }


  signupForm() {
    console.log('submit form....', this.myFormGroup.value);
    /// checking for server ...
      // if we got failure
      // setTimeout( () => {
      //   this.myFormGroup.setErrors({
      //     invalidLogin: true
      //   })
      // }, 2000)

      

  }
}
