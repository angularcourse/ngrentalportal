import { AbstractControl, AsyncValidator, ValidationErrors } from "@angular/forms";


export class UsernameValidators {

    static cannotContainSpace(control: AbstractControl): ValidationErrors | null {
        if((control.value as string).indexOf(' ') >= 0)
        return { cannotContainSpace : true}

        return null
    }


    static isUsernameAlreadyExists(control: AbstractControl) : Promise<ValidationErrors | null> {

            return new Promise( (resolve, reject) => {
                setTimeout( () => {
                    if(control.value === 'john')
                        resolve({ isUsernameAlreadyExists: true });
                        else
                  return resolve(null);
                }, 4000);
            })


    }
}