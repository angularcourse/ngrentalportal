import { PostService } from './../posts/post.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { combineLatest } from 'rxjs';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit {

  postId = 1;
  postDetail: any = undefined;
  title:string;
  flag : boolean = true
  constructor(private postService: PostService, private route: ActivatedRoute ) { }

  ngOnInit(): void {


    //trying to subscribe multiple observables at a time
    combineLatest( [ 
        this.route.paramMap,
        this.route.queryParamMap
    ]).subscribe( combined => {
      console.log('combined params ', combined);
          let paramMap = combined[0]
          this.postId = +paramMap.get('id');
          this.title = paramMap.get('title');
          
          let queryParamMap =  combined[1];
          console.log('queryParams', queryParamMap.get('userId'));
          let userId = +queryParamMap.get('userId');

          this.postService.getPostDetail(this.postId)
          .subscribe( response => {
            this.postDetail = response;
            console.log('postDetail', this.postDetail);

          }, error => {
              console.log('error', error)
          });
          
    })
    
  //   this.route.paramMap
  //   .subscribe( params => {
  //     console.log('params', params);
  //       this.postId = +params.get('id');
  //       this.title = params.get('title');
  //       this.postService.getPostDetail(this.postId)
  //   .subscribe( response => {
  //     this.postDetail = response;
  //   }, error => {
  //       console.log('error', error)
  //   });

  //   })

  // this.route.queryParamMap
  // .subscribe( queryParams => {
  //   console.log('queryParams', queryParams);
  // })
  }

}
